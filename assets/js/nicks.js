const names = [
  "student",
  "web developer",
  "web designer",
  "gamer",
  "GitHub addict",
  "coder",
  "sceptic",
  "editor",
  "data lover",
  "optimist",
  "blogger",
  "team leader",
  "perfectionist",
  "wannabe security expert",
  "Firefox user",
  "Apple enthusiast",
  "Open Source lover",
];
export default names;
